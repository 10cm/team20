﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Team20.Scene;
using Team20.Device;
using Team20.Def;

namespace Team20.Actor
{
    class Sword : Equipment
    {
        public Sword(IGameObjectMediator mediator, Player parent) : base(mediator, parent, "sword", "sword", 1, 1, 1, 1)
        {

        }

        public override void Attack()
        {
            Vector2 adjust = new Vector2(32, 16);
            if (parent.GetDirection() == Direction.Left)
            {
                adjust = new Vector2(-32, 16);
            }
            mediator.AddGameObject(new AttackCollision(parent.GetPosition() + adjust, 32, 32, GameDevice.Instance()));
        }

        public override void Update(GameTime gameTime)
        {

        }
    }
}
