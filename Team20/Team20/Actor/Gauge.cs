﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team20.Device;
using Team20.Actor;

namespace Team20.Actor
{
    class Gauge
    {

        float maxValue;
        public float currentValue;
        float m_width;
        Rectangle bounds;
        Color color;
        private Tower tower;
        private GameObject gameObject;
         
        public Gauge( Rectangle bounds,float currentValue ,float maxValue,float width,Color color,Tower tower)            
        {
            this.bounds = bounds;
            this.currentValue = currentValue;
            this.maxValue = maxValue;
            m_width = width;
            this.color = color;
            this.tower = tower;
        }

        public void Update(GameTime gameTime)
        {
            if (tower.IsHit())
            {
                currentValue=tower.GetHp();
            }
        }
        
        public void Draw(Renderer renderer)
        {
             

            int width = (int)((currentValue /maxValue)*m_width);
            //for (int i=1; i<=maxValue;i++)
            //{
            //    if(i<=currentValue)
            //    {
            //        renderer.DrawTexture("gauge", new Rectangle(bounds.X, bounds.Y, width, bounds.Height), color);
            //    }
            //    else
            //    {
            //        renderer.DrawTexture("pixel", bounds, Color.White);
            //    }
            //}
            renderer.DrawTexture("pixel", bounds, Color.Red);
            renderer.DrawTexture("pixel", new Rectangle(bounds.X, bounds.Y, width, bounds.Height), color);

            renderer.DrawTexture("gauge", bounds, Color.White);


        }


        
    }
}
