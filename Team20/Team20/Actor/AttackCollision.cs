﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Team20.Device;

namespace Team20.Actor
{
    class AttackCollision : GameObject
    {
        private int count;

        public AttackCollision(Vector2 position, int width, int height, GameDevice gameDevice) : base("", position, width, height, gameDevice)
        {
            isDeadFlag = false;
        }

        public AttackCollision(AttackCollision other) : this(other.position, other.width, other.height, other.gameDevice)
        { }

        public override object Clone()
        {
            return new AttackCollision(this);
        }

        public override void Hit(GameObject gameObject)
        {

        }

        public override void Initialize()
        {
            isDeadFlag = false;
        }

        public override void Update(GameTime gameTime)
        {
            count++;
            if (count > 10)
            {
                isDeadFlag = true;
            }
        }

        public override void Draw(Renderer renderer)
        {
            renderer.DrawTexture("block2", position + gameDevice.GetDisplayModify());
        }
    }
}
