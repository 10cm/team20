﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team20.Device;
using Team20.Scene;

namespace Team20.Actor
{
    class Grass:GameObject
    {
        IGameObjectMediator mediator;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="position">位置</param>
        /// <param name="gameDevice">ゲームデバイス</param>
        public Grass(Vector2 position, GameDevice gameDevice)
            : base("kusa", position, 32, 32, gameDevice)
        {
        }
        /// <summary>
        /// コピーコンストラクタ
        /// </summary>
        /// <param name="other"></param>
        public Grass(Grass other)
            : this(other.position,other.gameDevice)
        {

        }

        /// <summary>
        /// 複製
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new Grass(this);
        }

        /// <summary>
        /// ヒット通地
        /// </summary>
        /// <param name="gameObject">相手のオブジェクト</param>
        public override void Hit(GameObject gameObject)
        {
            //mediator.AddGameObject(
            //    new Money(
            //    position, gameDevice));
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime">ゲーム時間</param>
        public override void Update(GameTime gameTime)
        {

        }

        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize()
        {

        }
    }
}
