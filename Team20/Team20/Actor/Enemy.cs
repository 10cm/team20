﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Team20.Def;
using Team20.Scene;
using Team20.Device;
using Team20.Util;

namespace Team20.Actor
{
    class Enemy : GameObject
    {
        private Vector2 velocity;
        private IGameObjectMediator mediator;
        private bool isJump;
        private int hp;
        private Vector2 size;
        private Random rnd = new Random();
        private Timer timer, timer2;
        private TimerUI timerUI, timerUI2;
        private bool istime;
        private Direction dir;
        private int anime;
        
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="position"></param>
        /// <param name="gameDevice"></param>
        public Enemy(Vector2 position, GameDevice gameDevice, IGameObjectMediator mediator)
            : base("slime", position, 32, 32, gameDevice)
        {
            velocity = Vector2.Zero;
            this.mediator = mediator;
            hp = 1;
            isJump = true;
            size = new Vector2(32, 32);
            istime = false;
        }

        /// <summary>
        /// コピーコンストラクタ
        /// </summary>
        /// <param name="other"></param>
        public Enemy(Enemy other)
            : this(other.position, other.gameDevice, other.mediator)
        {

        }

        /// <summary>
        /// 複製
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new Enemy(this);
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer"></param>
        public override void Draw(Renderer renderer)
        {
            int sx = 0, sy = 0;

            sx = anime / 15;
            if(sx==2)
            {
                sx = 1;
            }
            sy = 0;
            sx *= (int)size.X;
            sy *= (int)size.Y;
            Rectangle rect = new Rectangle(sx, sy, (int)size.X, (int)size.Y);
            if (dir==Direction.Right)
            {
                renderer.DrawTexture(name, position + gameDevice.GetDisplayModify(), rect);
            }
            else
            {
                renderer.DrawTexture(name, position + gameDevice.GetDisplayModify(), rect);
            }

            

        }

        /// <summary>
        /// ヒット通知
        /// </summary>
        /// <param name="gameObject"></param>
        public override void Hit(GameObject gameObject)
        {
            Direction dir = this.CheckDirection(gameObject);

            if (gameObject is Block||gameObject is Grass)
            {
                if (dir == Direction.Top)
                {
                    position.Y = gameObject.GetRectangle().Top - this.height;
                    velocity.Y = 0.0f;
                }
                else if (dir == Direction.Right)
                {
                    position.X = gameObject.GetRectangle().Right;
                }
                else if (dir == Direction.Left)
                {
                    position.X = gameObject.GetRectangle().Left - this.width;
                }
                else if (dir == Direction.Bottom)
                {
                    position.Y = gameObject.GetRectangle().Bottom;
                }

            }
            if (gameObject is AttackCollision)
            {
                velocity.Y = -0.3f;
                velocity.X = 0.4f;
                isJump = true;
                hp -= 1;
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            MoveRight();
            MoveLeft();
            AnimetionUpdate();
            Console.WriteLine(hp);
            //落下処理（プレイヤーよりゆっくり落ちる）
            velocity.Y = (velocity.Y > 8.0f) ? (8.0f) : (velocity.Y + 0.2f);

            //プレイヤーが死んでいない時
            if (!mediator.IsPlayerDead())
            {
                Vector2 tempPlayerPosition = Vector2.Zero;
                //プレイヤー情報の取得
                if (mediator.GetTower() != null)
                {
                    tempPlayerPosition = mediator.GetTower().GetPosition();
                }
                velocity.X = 0.0f;

                velocity.X = 1.0f;//右移動を予定
                if (tempPlayerPosition.X+48 < position.X)
                {
                    velocity.X = -1.0f;
                }

                position += velocity;
            }

            if(hp<=0)
            {
                isDeadFlag = true;
            }

            //デバッグ用コマンド
            //Kを押したら死亡させる
            if(Input.GetKeyTrigger(Keys.K))
            {
                isDeadFlag = true;
            }

            ////画面外に出たら死亡
            //if (position.Y > Screen.Height)
            //{
            //    isDeadFlag = true;
            //}
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize()
        {
            dir = Direction.Right;
            anime = 0;
        }

        private void MoveRight()
        {
            dir = Direction.Right;
        }

        private void MoveLeft()
        {
            dir = Direction.Left;
        }

        private void AnimetionUpdate()
        {
            anime++;
            if(anime>=15*3)
            {
                anime = 0;
            }
        }
        //public override void Attack(GameObject gameObject)
        //{
        //    if(gameObject is Tower)
        //    {
        //        if()
        //    }
        //}

    }
}
