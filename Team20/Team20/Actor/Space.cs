﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team20.Device;

namespace Team20.Actor
{

    class Space : GameObject
    {/// <summary>
     /// コンストラクタ
     /// </summary>
     /// <param name="name"></param>
     /// <param name="position">位置</param>
     /// <param name="width"></param>
     /// <param name="height"></param>
     /// <param name="gameDevice">ゲームデバイス</param>
        public Space(Vector2 position, GameDevice gameDevice)
            : base("", position, 32, 32, gameDevice)
        {
        }

        /// <summary>
        /// コピーコンストラクタ
        /// </summary>
        /// <param name="other"></param>
        public Space(Space other)
            : this(other.position, other.gameDevice)
        {

        }

        /// <summary>
        /// 複製
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new Space(this);
        }

        /// <summary>
        /// ヒット通知
        /// </summary>
        /// <param name="gameObject">相手のオブジェクト</param>
        public override void Hit(GameObject gameObject)
        {

        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer"></param>
        public override void Draw(Renderer renderer)
        {

        }

        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize()
        {

        }
    }
}
