﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Team20.Device;
using Team20.Scene;

namespace Team20.Actor
{
    class Money : GameObject
    {
        private int money;//お金
        private IGameObjectMediator mediator;
        private Random rnd;//ドロップするお金をランダムで
        private GameObject gameObject;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="position"></param>
        /// <param name="gameDevice"></param>
        public Money(Vector2 position,GameDevice gameDevice) 
            : base("koin",position,32 , 32, gameDevice)
        {
            money = 0;
            //this.position = position;
            rnd = new Random();
        }

        /// <summary>
        /// コピーコンストラクタ
        /// </summary>
        /// <param name="other"></param>
        public Money(Money other)
            :this(other.position,other.gameDevice)
        { }

        /// <summary>
        /// 複製
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new Money(this);
        }

        /// <summary>
        /// 喰いこみ修正
        /// </summary>
        /// <param name="other"></param>
        public override void CorrectPosition(GameObject other)
        {
            base.CorrectPosition(other);
        }

        //public override void Draw(Renderer renderer)
        //{
        //    renderer.DrawTexture("koin",gameObject.GetPosition()*32);
        //    renderer.DrawTexture("money", new Vector2(1800, 1080));
        //    renderer.DrawTexture("number", new Vector2(1850, 1080),GetMoney());
        //}

        /// <summary>
        /// ヒット通知
        /// </summary>
        /// <param name="gameObject"></param>
        public override void Hit(GameObject gameObject)
        {
            if(gameObject is Player)
            {
                isDeadFlag = true;
                money++;
            }
            
        }

        /// <summary>
        /// 所持金取得
        /// </summary>
        /// <returns></returns>
        public int GetMoney()
        {
            return money;
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize()
        {
            rnd = new Random();

            money = 0;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            
        }

       
    }
}
