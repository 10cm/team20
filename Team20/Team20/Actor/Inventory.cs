﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Team20.Device;

namespace Team20.Actor
{
    class Inventory
    {
        private readonly int slotLimit;
        private int currentSlot;

        private List<Equipment> equipments;

        public Inventory()
        {
            slotLimit = 2;
            currentSlot = 0;
            equipments = new List<Equipment>();
            for (int i = 0; i < slotLimit; i++)
            {
                equipments.Add(null);
            }
        }

        public void slotControll()
        {
            //マウス処理
            if (Input.GetMouseWheel() > 0)
                currentSlot += 1;
            if (Input.GetMouseWheel() < 0)
                currentSlot -= 1;

            //超過処理
            if (currentSlot >= slotLimit)
                currentSlot = 0;
            if (currentSlot < 0)
                currentSlot = slotLimit - 1;

            //ボタン処理
            if (Input.GetKeyTrigger(Keys.D1))
                currentSlot = 0;
            if (Input.GetKeyTrigger(Keys.D2))
                currentSlot = 1;

            //if()
        }

        public void SetEquipment(Equipment equipment, int slotIndex)
        {
            equipments[slotIndex] = equipment;
        }

        public Equipment GetCurrentEquipment()
        {
            return equipments[currentSlot];
        }

        public int GetSlotLimit()
        {
            return slotLimit;
        }

        public void Update(GameTime gameTime)
        {
            slotControll();
        }

        public void Draw(Renderer renderer)
        {
            for (int i = 0; i < slotLimit; i++)
            {
                if (currentSlot == i)
                {
                    renderer.DrawTexture("inventory", new Vector2(900 + i * 62, 900));
                }
                else
                {
                    renderer.DrawTexture("inventory", new Vector2(900 + i * 62, 900), 0.1f);
                }
                if (i >= equipments.Count || equipments[i] == null)
                {
                    continue;
                }
                renderer.DrawTexture(equipments[i].IconName, new Vector2(900 + i * 62, 900));
            }
        }
    }
}
