﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Team20.Device;

namespace Team20.Actor
{
    class Tower : GameObject
    {
        private int hp;
        private int cnt;
        private int prehp;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="gameDevice"></param>
        public Tower(Vector2 position, GameDevice gameDevice) : base("tow", position, 128, 384, gameDevice)
        {
            isDeadFlag = false;
            hp = 100;
        }

        /// <summary>
        /// コピーコンストラクタ
        /// </summary>
        /// <param name="other"></param>
        public Tower(Tower other) : this(other.position, other.gameDevice) { }

        /// <summary>
        /// 複製
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new Tower(this);
        }

        /// <summary>
        /// ヒット通知
        /// </summary>
        /// <param name="gameObject"></param>
        public override void Hit(GameObject gameObject)
        {
            if (gameObject is Enemy)
            {
                cnt++;
                if (cnt > 60)
                {
                    hp--;
                    cnt = 0;
                }
            }
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize()
        {
            isDeadFlag = false;
            hp = 100;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            prehp = hp;
            if (hp <= 0)
                isDeadFlag = true;

            if (cnt <= 0)
                cnt = 60;
            
        }

        /// <summary>
        /// HPの取得
        /// </summary>
        /// <returns></returns>
        public int GetHp()
        {
            return hp;
        }

        public bool IsHit()
        {
            if(prehp-hp<=0)
            {
                return true;
            }
            return false;
        }
    }
}
