﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Team20.Def;
using Team20.Device;
using Team20.Scene;

namespace Team20.Actor
{
    class Player : GameObject
    {
        private Vector2 velocity;
        private IGameObjectMediator mediator;
        private int hp;
        private int cnt;
        private Vector2 size;
        private Vector2 halfSize;
        private ActionMode mode;
        private int timer;
        private Direction dir;
        private int attackInterval;
        private Map map;
        private float speed;
        private bool isJump;
        GameObject gameObject;
        private Inventory inventory;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="position"></param>
        /// <param name="gameDevice"></param>
        /// <param name="mediator"></param>
        public Player(Vector2 position, GameDevice gameDevice, IGameObjectMediator mediator)
            : base("stand", position, 32, 64, gameDevice)
        {
            velocity = Vector2.Zero;
            this.mediator = mediator;
            isJump = true;
            hp = 10;
            cnt = 0;
            size = new Vector2(32, 64);
            halfSize = new Vector2(0, 0);

            dir = Direction.Right;

            inventory = new Inventory();
            inventory.SetEquipment(new Sword(mediator, this), 0);
            inventory.SetEquipment(new Bow(mediator, this), 1);
        }

        /// <summary>
        /// コピーコンストラクタ
        /// </summary>
        /// <param name="other"></param>
        public Player(Player other)
            : this(other.position, other.gameDevice, other.mediator)
        { }


        /// <summary>
        /// 複製
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new Player(this);
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer"></param>
        public override void Draw(Renderer renderer)
        {
            int sx = 0, sy = 0;
            switch (mode)
            {
                case ActionMode.Move:
                    sx = timer / 15;
                    if (sx == 3)
                    {
                        sx = 1;

                    }
                    if (dir == Direction.Right)
                    {
                        sy = 0;
                    }

                    else
                    {
                        sy = 1;
                    }


                    break;
                case ActionMode.Jump:
                    sx = timer / 1;
                    if (dir == Direction.Right)
                    {
                        sy = 0;
                    }

                    else
                    {
                        sy = 1;
                    }
                    break;
                case ActionMode.Stand:
                    sx = timer / 20;
                    sy = 0;
                    //if (dir == Direction.Right)
                    //{
                    //    sy = 0;
                    //}
                    //else
                    //{
                    //    sy = 1;
                    //}
                    break;
                case ActionMode.Dash:
                    sx = timer / 10;

                    if (dir == Direction.Right)
                    {
                        sy = 1;
                    }
                    else
                    {

                        sy = 0;
                    }
                    break;
                default:
                    break;
            }
            sx *= (int)size.X;
            sy *= (int)size.Y;
            Rectangle rect = new Rectangle(sx, sy, (int)size.X, (int)size.Y);

            if (dir == Direction.Right)
            {
                if (Input.GetKeyState(Keys.Right) ||
                Input.GetKeyState(PlayerIndex.One, Buttons.RightStick))
                {
                    if (mode == ActionMode.Jump)
                    {
                        renderer.DrawTexture("jump", position + gameDevice.GetDisplayModify(), rect);
                    }
                    else if (mode == ActionMode.Move)
                    {
                        renderer.DrawTexture("walk", position + gameDevice.GetDisplayModify(), rect);

                    }

                    else if (mode == ActionMode.Dash)
                    {
                        renderer.DrawTexture("dash", position + gameDevice.GetDisplayModify(), rect);
                    }
                }
                else if (mode == ActionMode.Jump)
                {
                    renderer.DrawTexture("jump", position + gameDevice.GetDisplayModify(), rect);
                }
                else
                {
                    renderer.DrawTexture("stand", position + gameDevice.GetDisplayModify(), rect);

                }

            }
            else
            {

                if (Input.GetKeyState(Keys.Left) ||
                Input.GetKeyState(PlayerIndex.One, Buttons.LeftStick))
                {
                    if (velocity.Y < 0)
                    {
                        renderer.DrawTexture("jump", position + gameDevice.GetDisplayModify(), rect);
                    }
                    else if (position.Y >= 0)
                    {
                        if (mode == ActionMode.Move)
                        {
                            renderer.DrawTexture("walk", position + gameDevice.GetDisplayModify(), rect);
                        }
                        else if (mode == ActionMode.Dash)
                        {
                            rect.X = 192 - rect.X - (int)size.X;
                            renderer.DrawTexture("dash", position + gameDevice.GetDisplayModify(), rect);
                        }

                    }

                }
                else if (velocity.Y < 0)
                {
                    renderer.DrawTexture("jump", position + gameDevice.GetDisplayModify(), rect);
                }
                else
                {
                    renderer.DrawTexture("stand", position + gameDevice.GetDisplayModify(), rect);

                }

            }
            inventory.Draw(renderer);

        }

        /// <summary>
        /// ヒット通知
        /// </summary>
        /// <param name="gameObject"></param>
        public override void Hit(GameObject gameObject)
        {
            if (gameObject is Block || gameObject is Grass)
            {
                HitBlock(gameObject);
            }

            if (gameObject is Enemy)
            {
                cnt++;
                if (cnt > 60)
                {
                    hp--;
                    cnt = 0;
                }
            }

        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            timer++;
            ChangeMode();
            AnimationUpdate();
            JumpStart();
            inventory.Update(gameTime);
            if (isJump == true)
            {
                mode = ActionMode.Jump;
                timer = 0;
            }
            if (mode == ActionMode.Jump)
            {
                //ジャンプ中だけ落下
                velocity.Y = velocity.Y + 0.4f;
                //落下速度制限
                velocity.Y = (velocity.Y > 16.0f) ? (16.0f) : (velocity.Y);

            }

            if (velocity.X > 0 && dir != Direction.Right)
            {
                dir = Direction.Right;
            }
            else if (velocity.X < 0 && dir != Direction.Left)
            {
                dir = Direction.Left;
            }

            position = position + velocity;

            //Console.WriteLine(hp);

            attackInterval++;
            if (attackInterval > 30)
            {
                if (Input.GetKeyState(Keys.X))
                {
                    attackInterval = 0;
                    // 右位置補正
                    Vector2 adjust = new Vector2(32, 16);
                    if (dir == Direction.Left)
                    {
                        adjust = new Vector2(-32, 16);
                    }
                    var attackCollision = new AttackCollision(position + adjust, 16, 16, GameDevice.Instance());
                    mediator.AddGameObject(attackCollision);
                }
            }
            if (hp <= 0)
            {
                isDeadFlag = true;
            }
            InScreen();
            setDisplayModify();
            DashUpdate();

        }

        /// <summary>
        /// プレイヤーとブロックとの衝突面処理
        /// </summary>
        /// <param name="gameObject"></param>
        private void HitBlock(GameObject gameObject)
        {
            //当たった方向の取得
            Direction dir = this.CheckDirection(gameObject);
            //ブロックの上面と衝突
            if (dir == Direction.Top)
            {
                if (position.Y > 0.0f)
                {
                    position.Y = gameObject.GetRectangle().Top - this.height;
                    velocity.Y = 0.0f;
                    isJump = false;
                }
            }
            else if (dir == Direction.Right)//右
            {
                position.X = gameObject.GetRectangle().Right;
            }
            else if (dir == Direction.Left)//左
            {
                position.X = gameObject.GetRectangle().Left - width;
            }
            else if (dir == Direction.Bottom)//下
            {
                position.Y = gameObject.GetRectangle().Bottom;
            }
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize()
        {
            position = new Vector2(Screen.Width);
            timer = 0;
            mode = ActionMode.Stand;

        }

        /// <summary>
        /// 画面ズレ修正
        /// </summary>
        private void setDisplayModify()
        {
            //中心で描画するよう補正値を設定
            gameDevice.SetDisplayModify(new Vector2(-position.X + (Screen.Width / 2 - width / 2), 0.0f));
            //Playerのx座標が画面の中心より左なら見切れてるのでVector2.Zeroで設定しなおす
            if (position.X < Screen.Width / 2 - width / 2)
            {
                gameDevice.SetDisplayModify(Vector2.Zero);
            }
            if (position.X > Screen.Width + (Screen.Width / 2 - width / 2))
            {
                gameDevice.SetDisplayModify(new Vector2(-Screen.Width - 16, 0));
            }
        }

        public Vector2 GetVelocity()
        {
            return velocity;
        }

        private void ChangeMode()
        {


            if (Input.GetKeyState(Keys.Right) ||
                Input.GetKeyState(PlayerIndex.One, Buttons.RightStick) ||
                Input.GetKeyState(Keys.Left) ||
                Input.GetKeyState(PlayerIndex.One, Buttons.LeftStick))
            {
                if (mode == ActionMode.Stand)
                {
                    mode = ActionMode.Move;
                    timer = 0;

                }
            }
            else if (Input.GetKeyState(Keys.Space))
            {
                if (mode == ActionMode.Stand)
                {
                    mode = ActionMode.Jump;
                    timer = 0;
                }
            }
            else
            {
                if (mode == ActionMode.Move)
                {
                    mode = ActionMode.Stand;
                    timer = 0;
                }
                else if (mode == ActionMode.Dash)
                {
                    mode = ActionMode.Stand;
                    timer = 0;

                }
                else if (mode == ActionMode.Jump)
                {
                    mode = ActionMode.Stand;
                    timer = 0;
                }


            }
        }

        private void AnimationUpdate()
        {
            timer++;
            switch (mode)
            {
                case ActionMode.Move:
                    if (timer >= 15 * 4)
                    {
                        timer = 0;
                    }
                    break;
                case ActionMode.Jump:
                    if (timer >= 1)
                    {
                        timer = 0;
                    }
                    break;
                case ActionMode.Stand:
                    if (timer >= 20 * 2)
                    {
                        timer = 0;
                    }
                    break;
                case ActionMode.Dash:
                    if (timer >= 10 * 5)
                    {
                        timer = 0;
                    }
                    break;
                default:
                    break;
            }
        }

        private void InScreen()
        {

            Vector2 min = halfSize;

            Vector2 max = new Vector2(Screen.Width * 2 + 32 * 30 - Screen.Width / 2 - width / 2, Screen.Height) - halfSize;
            position = Vector2.Clamp(position, min, max);

        }
        void DashUpdate()
        {
            speed = 8.0f;
            if (Input.GetKeyState(Keys.Z))
            {
                speed = 16.0f;
            }
            if (speed < 9.0f)
            {
                mode = ActionMode.Move;
            }
            else
            {
                mode = ActionMode.Dash;
            }
            velocity.X = Input.Velocity().X * speed;
        }

        void JumpStart()
        {
            if (mode == ActionMode.Jump)
            {
                return;
            }

            if (isJump == false && (Input.GetKeyTrigger(Keys.Space) ||
                Input.GetKeyTrigger(PlayerIndex.One, Buttons.B)))
            {
                mode = ActionMode.Jump;
                timer = 0;
                isJump = true;
                velocity.Y = -8.0f;
            }
        }

        public Direction GetDirection()
        {
            return dir;
        }
    }
}
