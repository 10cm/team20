﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Team20.Def;
using Team20.Scene;
using Team20.Device;
using Team20.Util;

namespace Team20.Actor
{
    class BossEnemy : GameObject
    {
        private Vector2 velocity;
        private IGameObjectMediator mediator;
        private int hp;
        private Vector2 size;
        private int anime;
        private Direction dir;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="position"></param>
        /// <param name="gameDevice"></param>
        /// <param name="mediator"></param>
        public BossEnemy(Vector2 position, GameDevice gameDevice, IGameObjectMediator mediator)
            : base("eyeof", position,256,256, gameDevice)
        {
            velocity = Vector2.Zero;
            this.mediator = mediator;
            hp = 30;
            size = new Vector2(256, 256);
        }
        /// <summary>
        /// コピーコンストラク
        /// </summary>
        /// <param name="other"></param>
        public BossEnemy(BossEnemy other)
            : this(other.position, other.gameDevice,
                  other.mediator)
        {

        }
        /// <summary>
        /// 複製
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new BossEnemy(this);
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            AnimetionUpdate();
            //落下処理
            velocity.Y = (velocity.Y > 8.0f) ? (8.0f) : (velocity.Y + 0.2f);
            if (hp>=15)
            {
                Vector2 tempPlayerPosition = Vector2.Zero;
                //タワー情報の取得
                if (mediator.GetTower() != null)
                {
                    tempPlayerPosition = mediator.GetTower().GetPosition();
                }
                velocity.X = 0.0f;

                velocity.X = 1.0f;
                if (tempPlayerPosition.X + 48 < position.X)
                {
                    velocity.X = 1.0f;
                }
                position += velocity;
            }
            else if(hp<15)
            {
                Vector2 tempplayerPosition = Vector2.Zero;
                //プレイヤーの取得
                if(mediator.GetPlayer()!=null)
                {
                    tempplayerPosition = mediator.GetPlayer().GetPosition();
                }
                velocity.X = 0.0f;

                velocity.X=1.0f;
                if(tempplayerPosition.X+48<position.X)
                {
                    velocity.X = 1.0f;
                }
                position += velocity;
            }
            else if(hp <= 0)
            {
                isDeadFlag = true;
            }
        }
        /// <summary>
        /// ヒット通知
        /// </summary>
        /// <param name="gameObject"></param>
        public override void Hit(GameObject gameObject)
        {
            Direction dir = this.CheckDirection(gameObject);
            if (gameObject is Block)
            {
                if (dir == Direction.Top)
                {
                    position.Y = gameObject.GetRectangle().Top - this.height;
                    velocity.Y = 0.0f;
                }
                else if (dir == Direction.Right)
                {
                    position.X = gameObject.GetRectangle().Right;
                }
                else if (dir == Direction.Left)
                {
                    position.X = gameObject.GetRectangle().Left - this.width;
                }
                else if (dir == Direction.Bottom)
                {
                    position.Y = gameObject.GetRectangle().Bottom;
                }
            }
            if (gameObject is AttackCollision)
            {
                velocity.Y = -0.3f;
                velocity.X = 0.4f;
                hp -= 1;
            }
        }
        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize()
        {
            dir = Direction.Right;
        }
        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer"></param>
        public override void Draw(Renderer renderer)
        {
            int sx = 0, sy = 0;

            sx = anime / 15;
            if (sx == 8)
            {
                sx = 1;
            }
            sy = 0;
            sx *= (int)size.X;
            sy *= (int)size.Y;
            Rectangle rect = new Rectangle(sx, sy, (int)size.X, (int)size.Y);
            if (dir == Direction.Right)
            {
                renderer.DrawTexture(name, position + gameDevice.GetDisplayModify(), rect);
            }
            else
            {
                renderer.DrawTexture(name, position + gameDevice.GetDisplayModify(), rect);
            }
        }

        private void AnimetionUpdate()
        {
            anime++;
            if (anime >= 15 * 9)
            {
                anime = 0;
            }
        }
    }
}
