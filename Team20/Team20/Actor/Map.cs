﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Team20.Device;
using Team20.Util;

namespace Team20.Actor
{
    /// <summary>
    /// マップクラス
    /// </summary>
    class Map
    {
        private List<List<GameObject>> mapList;
        private GameDevice gameDevice;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="gameDevice"></param>
         public Map(GameDevice gameDevice)
        {
            mapList = new List<List<GameObject>>();
            this.gameDevice = gameDevice;
        }

        /// <summary>
        /// ブロック追加
        /// </summary>
        /// <param name="lineCnt"></param>
        /// <param name="line"></param>
        /// <returns></returns>
        private List<GameObject> addBlock(int lineCnt, string[] line)
        {

            // コピー元オブジェクト登録用でディクショナリ
            Dictionary<string, GameObject> objectList = new Dictionary<string, GameObject>();
            objectList.Add("0", new Space(Vector2.Zero, gameDevice));
            //スペースは0
            objectList.Add("1", new Block(Vector2.Zero, gameDevice));
            objectList.Add("2", new Grass(Vector2.Zero, gameDevice));
            //ブロックは1

            //作業用リスト
            List<GameObject> workList = new List<GameObject>();
            //列カウント用
            int colCnt = 0;
            //渡された1行から1つずつ作業リストに登録
            foreach (var s in line)
            {
                try
                {
                    //ディクショナリから元データを取り出し、クローン機能で複製
                    GameObject work = (GameObject)objectList[s].Clone();
                    work.SetPosition(new Vector2(colCnt * work.GetHeight(), lineCnt * work.GetWidth()));
                    workList.Add(work);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                //列カウントを増やす
                colCnt += 1;
            }
            return workList;
        }

        /// <summary>
        /// Mapの読み込み
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="path"></param>
        public void Load(string filename, string path = "./")
        {
            CSVReader csvReader = new CSVReader();
            csvReader.Read(filename, path);

            var date = csvReader.GetData();

            for (int lineCnt = 0; lineCnt < date.Count(); lineCnt++)
            {
                mapList.Add(addBlock(lineCnt, date[lineCnt]));
            }
        }

        /// <summary>
        ///マップリストのクリア
        /// </summary>
        public void Unload()
        {
            mapList.Clear();
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            foreach (var list in mapList)
            {
                foreach (var obj in list)
                {
                    //objがSpaceクラスのオブジェクトなら次へ
                    if (obj is Space)
                    {
                        continue;
                    }
                    //更新
                    obj.Update(gameTime);
                }
            }
        }

        /// <summary>
        /// ヒット通知
        /// </summary>
        /// <param name="gameObject"></param>
        public void Hit(GameObject gameObject)
        {
            Point work = gameObject.GetRectangle().Location;
            //配列の何行何列目にいるかを計算
            int x = work.X / 32;
            int y = work.Y / 32;

            //移動で喰いこんでいる時の修正
            if (x < 1)
            {
                x = 1;
            }
            if (y < 1)
            {
                y = 1;
            }

            Range yRange = new Range(0, mapList.Count() - 1);
            Range xRange = new Range(0, mapList[0].Count() - 1);


            for (int row = y - 1; row <= (y + 8); row++)
            {
                for (int col = x - 1; col <= (x + 8); col++)
                {
                    //配列外ならなにもしない
                    if (xRange.IsOutOfRange(col) || yRange.IsOutOfRange(row))
                    {
                        continue;
                    }
                    GameObject obj = mapList[row][col];
                    //objがSpaceクラスのオブジェクトなら次へ
                    if (obj is Space)
                    {
                        continue;
                    }
                    //衝突判定
                    if (obj.IsCollision(gameObject))
                    {
                        gameObject.Hit(obj);
                    }
                }
            }
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer"></param>
        public void Draw(Renderer renderer)
        {
            //foreachの2重ループでmaplistに登録されている
            foreach (var list in mapList)
            {
                foreach (var obj in list)
                {
                    if (obj is Space)
                    {
                        continue;
                    }
                    obj.Draw(renderer);
                }
            }

        }
    }
}
