﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Team20.Scene;
using Team20.Device;
using Microsoft.Xna.Framework.Input;

namespace Team20.Actor
{
    class EqipmentNanager
    {
        List<Equipment> equipment;
        List<string> m_menuItem;//オプション
        List<string> m_menutext;//文字
        List<Vector2> m_position;//オプションの位置
        List<Vector2> m_textposition;//文字列の壱
        
        //Color m_selectedColor;//選択の色
        //Color m_unselectedColor;//選択されていないオプションの色
        int m_currentMenuItem;//現在されているオプション
        public EqipmentNanager()
        {
            Initialize();
           
        }

        public virtual void AddMenuItem(string name,Vector2 position)
        {
            m_menuItem.Add(name);
            m_position.Add(position);
        }

        public void AddMenuText(string text,Vector2 position)
        {
            m_menutext.Add(text);
            m_textposition.Add(position);
        }

        public void Addequipment(Equipment equipment)
        {

        }

        public void Draw(Renderer renderer)
        {
            //renderer.DrawTexture("hukidasi",getplayerposition)
            foreach (var i in m_menuItem)
            {
                foreach(var p in m_position)
                {
                    renderer.DrawTexture(i,p);
                }
                
            }
            //renderer.DrawTexture("button1", new Vector2(100,100));
            renderer.DrawTexture("cursor",m_position[m_currentMenuItem]);
            
        
        }

        public void AddEquipment(Equipment equipments)
        {
            if (equipment == null)
            {
                return;
            }
            equipment.Add(equipments);
        }

        public void Initialize()
        {
            equipment = new List<Equipment>();
            m_currentMenuItem = 0;
            m_menuItem = new List<string>();
            m_menutext = new List<string>();
            m_position = new List<Vector2>();
            m_textposition = new List<Vector2>();
            //m_selectedColor = selectedColor;
        }



        public void Update(GameTime gameTime)
        {
            foreach (var c in equipment)
            {
                equipment.Add(c);
            }

            foreach (var c in equipment)
            {
                if (Input.GetKeyTrigger(Keys.Space))
                {
                    c.Update(gameTime);

                }
                
            }

            if (Input.GetKeyTrigger(Keys.A))
            {
                m_currentMenuItem++;
                if(m_currentMenuItem>m_menuItem.Count)
                {
                    m_currentMenuItem = m_menuItem.Count;
                }
            }

            if (Input.GetKeyTrigger(Keys.D))
            {
                m_currentMenuItem--; ;
                if (m_currentMenuItem < 0)
                {
                    m_currentMenuItem = 0;
                }
            }
            

        }
    }
}
