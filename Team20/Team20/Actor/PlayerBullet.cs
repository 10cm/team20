﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Team20.Def;
using Team20.Device;
using Team20.Scene;
using Team20.Util;

namespace Team20.Actor
{
    class PlayerBullet : GameObject
    {
        private Vector2 velocity;
        private float roatation;
        private IGameObjectMediator mediator;

        public PlayerBullet(Vector2 position, GameDevice gameDevice, IGameObjectMediator mediator)
             : base("bullet", position, 32, 32, gameDevice)
        {
            velocity = Vector2.Zero;
            this.mediator = mediator;
        }
        
        public PlayerBullet(PlayerBullet other)
            :this(other.position,other.gameDevice,other.mediator)
        {

        }

        public override object Clone()
        {
            return new PlayerBullet(this);
        }

        public override void Draw(Renderer renderer)
        {
            renderer.DrawTexture(name, position + gameDevice.GetDisplayModify());
        }

        public override void Update(GameTime gameTime)
        {
            position += velocity * 7;
            Range range = new Range(0, Screen.Width);
            if(range.IsOutOfRange((int)position.X))
            {
                isDeadFlag = true;
            }
            range = new Range(0, Screen.Height);
            if(range.IsOutOfRange((int)position.Y))
            {
                isDeadFlag = true;
            }

            roatation -= 0.1f;
        }

        public override void Hit(GameObject gameObject)
        {
            Direction dir = this.CheckDirection(gameObject);

        }

        public override void Initialize()
        {

        }
    }


}

