﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Team20.Device;

namespace Team20.Actor
{
    class MoneyUI
    {
        private GameObject money;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="money"></param>
        public MoneyUI(GameObject money)
        {
            this.money = money;
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer"></param>
        public void Draw(Renderer renderer)
        {

            renderer.DrawTexture("money", new Vector2(1600, 10));
            renderer.DrawNumber("number", new Vector2(1750, 40),((Money)money).GetMoney());
        }
    }
}
