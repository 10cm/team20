﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Team20.Scene;
using Team20.Device;


namespace Team20.Actor
{
    abstract class Equipment
    {
        protected string name;
        public string Name { get { return name; } }

        protected string iconName;
        public string IconName { get { return iconName; } }
        protected int ID;
        protected int power;
        public int Power { get { return power; } }
        protected int attackSpeed;
        public int AttackSpeed { get { return attackSpeed; } }
        protected int price;

        public int Price { get { return price; } }
        protected IGameObjectMediator mediator;
        protected Player parent;

        public Equipment(IGameObjectMediator mediator, Player parent, string name, string iconName, int ID, int power, int attackSpeed, int price)
        {
            this.mediator = mediator;
            this.parent = parent;
            this.name = name;
            this.iconName = iconName;
            this.ID = ID;
            this.power = power;
            this.attackSpeed = attackSpeed;
            this.price = price;
        }

        public abstract void Update(GameTime gameTime);

        public abstract void Attack();
    }
}
