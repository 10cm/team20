﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Team20.Device;
using Team20.Def;

namespace Team20.Actor
{

    abstract class GameObject
    {
        protected string name;
        protected Vector2 position;
        protected int width;
        protected int height;
        protected bool isDeadFlag = false;
        protected GameDevice gameDevice;

        //protected int hp;//体力
        //protected int power;//攻撃力

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="name">アセット名</param>
        /// <param name="position">位置</param>
        /// <param name="width">幅</param>
        /// <param name="height">高さ</param>
        /// <param name="gameDevice">ゲームデバイス</param>
        public GameObject(string name, Vector2 position, int width, int height, GameDevice gameDevice)
        {
            this.name = name;
            this.position = position;
            this.width = width;
            this.height = height;
            this.gameDevice = gameDevice;

            //this.hp = hp;
        }

        /// <summary>
        /// 位置の設定
        /// </summary>
        /// <param name="position"></param>
        public void SetPosition(Vector2 position)
        {
            this.position = position;
        }

        /// <summary>
        /// 位置の取得
        /// </summary>
        /// <returns></returns>
        public Vector2 GetPosition()
        {
            return position;
        }

        /// <summary>
        /// オブジェクト幅の取得
        /// </summary>
        /// <returns></returns>
        public int GetWidth()
        {
            return width;
        }

        /// <summary>
        /// オブジェクトの高さの取得
        /// </summary>
        /// <returns></returns>
        public int GetHeight()
        {
            return height;
        }

        //抽象メソッド
        public abstract object Clone();                     //複製
        public abstract void Update(GameTime gameTime);     //更新
        public abstract void Hit(GameObject gameObject);    //ヒット通知
        public abstract void Initialize();

        //public abstract void Initialize();

        //仮想メソッド
        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer">描画オブジェクト</param>
        public virtual void Draw(Renderer renderer)
        {
            renderer.DrawTexture(name, position + gameDevice.GetDisplayModify());
        }

        /// <summary>
        /// 死んでいるか？
        /// </summary>
        /// <returns>死んでいたらtrue</returns>
        public bool IsDead()
        {
            return isDeadFlag;
        }

        /// <summary>
        /// 当たり判定用、矩形情報の取得
        /// </summary>
        /// <returns></returns>
        public Rectangle GetRectangle()
        {
            Rectangle area = new Rectangle();
            area.X = (int)position.X;
            area.Y = (int)position.Y;
            area.Height = height;
            area.Width = width;

            return area;
        }

        /// <summary>
        /// 矩形同士の当たり判定
        /// </summary>
        /// <param name="otherObj">相手の矩形</param>
        /// <returns></returns>
        public bool IsCollision(GameObject otherObj)
        {
            return this.GetRectangle().Intersects(otherObj.GetRectangle());
        }

        /// <summary>
        /// 向きの取得
        /// </summary>
        /// <param name="otherObj"></param>
        /// <returns></returns>
        public Direction CheckDirection(GameObject otherObj)
        {
            //中心位置の取得
            Point thisCenter = this.GetRectangle().Center;
            Point otherCenter = otherObj.GetRectangle().Center;

            //向きのベクトルを取得
            Vector2 dir = new Vector2(thisCenter.X, thisCenter.Y) - new Vector2(otherCenter.X, otherCenter.Y);

            //当たっている側面をリターンする
            //x成分とy成分でどちらのほうが量が多いか
            if (Math.Abs(dir.X) > Math.Abs(dir.Y))
            {
                //xの向きが正のとき
                if (dir.X > 0)
                {
                    return Direction.Right;
                }
                return Direction.Left;
            }

            //y成分が大きく、正の値か？
            if (dir.Y > 0)
            {
                return Direction.Bottom;
            }

            //プレイヤーがブロックに乗った
            return Direction.Top;
        }

        /// <summary>
        /// 喰いこみ修正
        /// </summary>
        /// <param name="other"></param>
        public virtual void CorrectPosition(GameObject other)
        {
            Direction dir = this.CheckDirection(other);
            if (dir == Direction.Top)
            {
                position.Y = other.GetRectangle().Top - this.height;
            }
            if (dir == Direction.Right)
            {
                position.X = other.GetRectangle().Right;
            }
            if (dir == Direction.Left)
            {
                position.X = other.GetRectangle().Left - this.width;
            }
            if (dir == Direction.Bottom)
            {
                position.Y = other.GetRectangle().Bottom;
            }

        }
    }
}
