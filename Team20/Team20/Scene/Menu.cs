﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Team20.Device;

namespace Team20.Scene
{
    class Menu :IScene
    {
        //private List<Vector2> positions;
        //private List<Rectangle> sourceRects;
        //private int cursor;
        //1つ前のシーン
        private IScene lastScene;

        private bool isEnd;

        private Scene next;

        private int cnt;
        private Sound sound;

        public Menu()
        {
            isEnd = false;
            var gameDevice = GameDevice.Instance();
            sound = gameDevice.GetSound();
            //isEnd = false;
            //var gameDevice = GameDevice.Instance();

            //positions = new List<Vector2>()
            //{
            //    new Vector2(64,128),
            //    new Vector2(480,128),
            //    new Vector2(896,128),
            //};

            //sourceRects = new List<Rectangle>()
            //{
            //    new Rectangle(0,0,320,180),
            //    new Rectangle(320,0,320,180),
            //    new Rectangle(640,0,320,180),
            //};
        }

        public void Draw(Renderer renderer)
        {
            lastScene.Draw(renderer);

            renderer.Begin();
            renderer.DrawTexture("Menu",new Vector2(0,0),0.5f);
            //for (int i = 0; i < 3; i = i + 1)
            //{
            //    renderer.DrawTexture("Menu",
            //        positions[i], sourceRects[i]);
            //}
            //renderer.DrawTexture("Menu", positions[cursor]);
            renderer.End();
        }

        //SceneBaseのInitialize
        public void Initialize(Scene lastSceneName)
        {
            isEnd = false;
            //次のシーン名設定
            next = lastSceneName;
            //cursor = 0;
            //カウント初期化
            cnt = 0;
        }
        //ISceneのInitialize
        public void Initialize()
        {

        }

        public void Update(GameTime gameTime)
        {
            
            if(Input.GetKeyTrigger(Keys.P))
            {
                isEnd = true;
            }

            //カウント
            cnt++;
            if(cnt>13)
            {
                cnt = 0;
            }

            //if (Input.GetKeyTrigger(Keys.Right))
            //{
            //    cursor += 1;
            //    if (cursor > 2)
            //        cursor = 2;
            //}
            //if (Input.GetKeyTrigger(Keys.Left))
            //{
            //    cursor -= 1;
            //    if (cursor < 0)
            //        cursor = 0;
            //}
            //if (Input.GetKeyTrigger(Keys.Space))
            //{
            //    isEnd = true;
            //    // GameData.stageNumber = cursor + 1;
            //}
        }

        public void Shutdown()
        {

        }
        
        public void SetLastScene(IScene lastScene)
        {
            this.lastScene = lastScene;
        }

        public bool IsEnd()
        {
            return isEnd;
        }

        public Scene Next()
        {
            return Scene.GamePlay;
            //if (Input.GetKeyTrigger(Keys.P))
                //return Scene.GamePlay;
        }
    }

}

