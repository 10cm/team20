﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Team20.Device;

namespace Team20.Scene
{
    /// <summary>
    /// タイトル画面
    /// </summary>
    class Title : IScene
    {
        private bool isEnd;
        private Scene next;
        private Sound sound;
       
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Title()
        {
            isEnd = false;
            var gameDevice = GameDevice.Instance();
            sound = gameDevice.GetSound();
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer"></param>
        public void Draw(Renderer renderer)
        {
            renderer.Begin();
            renderer.DrawTexture("Title", Vector2.Zero);
            renderer.End();
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public void Initialize(Scene lastName)
        {
            isEnd = false;
            //次のシーン名設定
            next = Scene.StageSelect;
        }

        public bool IsEnd()
        {
            return isEnd;
        }

        public Scene Next()
        {
            return next;
        }

        ///// <summary>
        ///// 次のシーンへ
        ///// </summary>
        ///// <returns>次のシーン</returns>
        //public Scene Next()
        //{
        //    return Scene.GamePlay;
        //}

        /// <summary>
        /// 終了
        /// </summary>
        public void Shutdown()
        {
            
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            sound.PlayBGM("bgm01");
            //sound.PlaySE("tap_SE");
            if (Input.GetKeyTrigger(Keys.Space))
            {
                isEnd = true;
                sound.PlaySE("tap_SE");
               
            }
        }
    }
}
