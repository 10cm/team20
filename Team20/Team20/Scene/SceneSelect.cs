﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Team20.Device;

namespace Team20.Scene
{
    class SceneSelect : IScene
    {
        private bool isEndFlag;
        private List<Vector2> positions;
        private List<Rectangle> sourceRects;
        private int cursor;
        /// <summary>
        /// コンストラクト
        /// </summary>
        public SceneSelect()
        {
            isEndFlag = false;
            var gameDevice = GameDevice.Instance();
            positions = new List<Vector2>()
            {
                new Vector2(64, 128),
                new Vector2(480, 128),
                new Vector2(896, 128),
            };
            sourceRects = new List<Rectangle>()
            {
                new Rectangle(0,0,320,180),
                new Rectangle(320,0,320,180),
                new Rectangle(640,0,620,180),
            };
        }
        /// <summary>
        /// 表示
        /// </summary>
        /// <param name="renderer"></param>
        public void Draw(Renderer renderer)
        {
            renderer.Begin();
            for (int i = 0; i < 3; i = i + 1)
            {
                renderer.DrawTexture("stage", positions[i], sourceRects[i]);
            }
            renderer.DrawTexture("cursor", positions[cursor]);
            renderer.End();
        }
        /// <summary>
        /// 初期化
        /// </summary>
        public void Initialize()
        {
            isEndFlag = false;
            cursor = 0;
        }

        public void Initialize(Scene lastName)
        {

        }

        public bool IsEnd()
        {
            return isEndFlag;
        }

        public Scene Next()
        {
            return Scene.GamePlay;
        }
        /// <summary>
        /// 終了処理
        /// </summary>
        public void Shutdown()
        {
            
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            if (Input.GetKeyTrigger(Keys.Right))
            {
                cursor += 1;
                if (cursor > 2)
                    cursor = 2;
            }
            if (Input.GetKeyTrigger(Keys.Left))
            {
                cursor -= 1;
                if (cursor < 0)
                    cursor = 0;
            }
            if (Input.GetKeyTrigger(Keys.Space))
            {
                isEndFlag = true;
                GameData.stageNumber = cursor + 1;
            }
        }
    }
}
