﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team20.Scene
{
    /// <summary>
    /// シーンの列挙型
    /// </summary>
  public  enum Scene
    {
        None,
        Title,          //タイトル画面
        Config,         //設定画面
        StageSelect,    //ステージ選択
        GamePlay,       //ゲームプレイ画面
        Menu,           //メニュー画面
        GameClear,　　　//ゲームオーバー
        GameOver,　　　 //ゲームクリア
        Result,         //結果表示画面
        Shop,           //ショップ画面
    }
}
