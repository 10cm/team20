﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team20.Device;

namespace Team20.Scene
{
    /// <summary>
    /// シーン管理者
    /// </summary>
    class SceneManager
    {
        //シーン管理のディクショナリ
        private Dictionary<Scene, IScene> scenes = new Dictionary<Scene, IScene>();
        //private Dictionary<Scene, IScene> iscenes;
        //現在のシーン
        private IScene currentScene;
        //今のシーン
        private Scene currentSceneName;
        //1つ前のシーン名
        private Scene lastSceneName;


        public SceneManager()
        {
            //Dictiionaryの生成
            scenes = new Dictionary<Scene, IScene>();
            //今のシーンをnullにしておく
            currentScene = null;

            //シーン名をなしで初期化
            currentSceneName = Scene.None;
            lastSceneName = Scene.None;

        }

        public void Initialize(Scene lastName)
        {
            AddScenes();
            //最初のシーン設定
            Change(Scene.Title);

            //next = lastSceneName;
            //メニューシーンなら
            if(currentScene is Menu)
            {
                ((Menu)currentScene).SetLastScene(scenes[lastSceneName]);
            }
        }

        /// <summary>
        /// シーンの追加
        /// </summary>
        /// <param name="name">シーン名</param>
        /// <param name="scene">具体的なシーンオブジェクト</param>
        public void Add(Scene name,IScene scene)
        {
            //すでにシーンが追加されていたら
            if(scenes.ContainsKey(name))
            {
                return;//何もせず
            }
            //ディクショナリに登録
            scenes.Add(name, scene);
        }

        public void Change(Scene name)
        {
            //何かシーンが登録されていたら
            if(currentScene!=null)
            {
                //現在のシーンの終了処理
                currentScene.Shutdown();
            }

            //ディクショナリから取り出し、現在のシーンに設定
            currentScene = scenes[name];
            //前のシーン名を設定
            lastSceneName = currentSceneName;
            //今もシーン名を設定
            currentSceneName = name;

            //シーンの初期化
           currentScene.Initialize(lastSceneName);

            if(currentScene is Menu)
            {
                ((Menu)currentScene).SetLastScene(scenes[lastSceneName]);
            }
        }

        public void Update(GameTime gameTime)
        {
            //シーンが登録されていない
            if(currentScene==null)
                return;//何もしない

            //現在のシーンの更新
            currentScene.Update(gameTime);
            //現在のシーンが終了しているか
            if(currentScene.IsEnd())
            {
                //次のシーンを取り出し、シーン切り替え
                Change(currentScene.Next());
            }
        }

        public void Draw(Renderer renderer)
        {
            if(currentScene==null)
            {
                return;
            }
            //シーンを描画
            currentScene.Draw(renderer);
        }

        public void AddScenes()
        {
            //シーン登録
            Add(Scene.Title, new Title());
            Add(Scene.GamePlay, new GamePlay());
            Add(Scene.GameOver, new GameOver());
            Add(Scene.Menu, new Menu());
        }
      
    }
}
