﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team20.Device;
using Microsoft.Xna.Framework;

namespace Team20.Scene
{
    /// <summary>
    /// シーンインタフェース 
    /// </summary>
    interface IScene
    {
        void Initialize(Scene lastName);//初期化
        void Update(GameTime gameTime); //更新
        void Draw(Renderer renderer);   //描画
        void Shutdown();                //終了

        bool IsEnd();                   //終了しているか？
        Scene Next();                   //次のシーンへ
    }
}
