﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Team20.Device;

namespace Team20.Scene
{
    /// <summary>
    /// ゲームクリア画面
    /// </summary>
    class GameClear : IScene
    {
        private bool isEnd;
        private Sound sound;
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public GameClear()
        {
            isEnd = false;
            var gameDevice = GameDevice.Instance();
            sound = gameDevice.GetSound();
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer"></param>
        public void Draw(Renderer renderer)
        {
            renderer.Begin();
            renderer.DrawTexture("GameClear", Vector2.Zero);
            renderer.End();
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="lastName"></param>
        public void Initialize(Scene lastName)
        {
            isEnd = false;
        }

        /// <summary>
        /// 終了しているか？
        /// </summary>
        /// <returns></returns>
        public bool IsEnd()
        {
            return isEnd;
        }

        /// <summary>
        /// 次のシーン
        /// </summary>
        /// <returns></returns>
        public Scene Next()
        {
            return Scene.Title;
        }

        /// <summary>
        /// 終了
        /// </summary>
        public void Shutdown()
        {
            sound.StopBGM();
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            sound.PlayBGM("bgm08");
            if (Input.GetKeyTrigger(Keys.Space))
            {
                isEnd = true;
            }
        }
    }
}
