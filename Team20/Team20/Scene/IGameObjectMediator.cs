﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team20.Actor;

namespace Team20.Scene
{
    /// <summary>
    /// ゲームオブジェクト仲介者クラス
    /// </summary>
    interface IGameObjectMediator
    {
        void AddGameObject(GameObject gameObject);  //ゲームオブジェクト追加
        GameObject GetPlayer();                     //プレイヤー取得
        GameObject GetTower();                      //タワー取得
        bool IsPlayerDead();                        //プレイヤーが死んでいるかどうか
    }
}
