﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Team20.Device;

namespace Team20.Scene
{
    /// <summary>
    /// ショップ画面
    /// </summary>
    class Shop : IScene
    {
        private bool isEnd;
        private IScene backGround;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Shop()
        {
            isEnd = false;
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer"></param>
        public void Draw(Renderer renderer)
        {
            backGround.Draw(renderer);

        }

        /// <summary>
        /// 初期化
        /// </summary>
        public void Initialize(Scene lastName)
        {
            isEnd = false;
        }

        /// <summary>
        /// 終了しているか？
        /// </summary>
        /// <returns></returns>
        public bool IsEnd()
        {
            return isEnd;
        }

        /// <summary>
        /// 次のシーン
        /// </summary>
        /// <returns></returns>
        public Scene Next()
        {
            return Scene.GamePlay;
        }

        /// <summary>
        /// 終了
        /// </summary>
        public void Shutdown()
        {

        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            backGround.Update(gameTime);
            if (Input.GetKeyTrigger(Keys.Escape))
            {
                isEnd = true;
            }
        }
    }
}
