﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Team20.Device;
using Team20.Util;

namespace Team20.Scene
{
    class TimerUI
    {
        private Timer timer;

        public TimerUI(Timer timer)
        {
            this.timer = timer;
        }

        public void Draw(Renderer renderer)
        {
            renderer.DrawNumber("number", new Vector2(960, 100), (int)timer.Now());
        }
    }
}
