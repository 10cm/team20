﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Team20.Device;
using Team20.Actor;
using Team20.Def;

namespace Team20.Scene
{
    /// <summary>
    /// ゲームプレイ画面
    /// </summary>
    class GamePlay : IScene
    {
        private bool isEnd;
        private Map map;
        private GameObjectManager gameObjectManager;
        private Player player;
        private Tower tower;
        private Enemy enemy;
        private Gauge gauge;
        private Scene next;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public GamePlay()
        {
            isEnd = false;
            gameObjectManager = new GameObjectManager();
            next = Scene.GameOver;
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="renderer"></param>
        public void Draw(Renderer renderer)
        {
            renderer.Begin();
            renderer.DrawTexture("BackGround", Vector2.Zero);
            map.Draw(renderer);
            gauge.Draw(renderer);
            gameObjectManager.Draw(renderer);
            renderer.End();
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public void Initialize(Scene lastName)
        {
            isEnd = false;
            //前のシーンがメニューだったら
            if (lastName == Scene.Menu)
                return; 

            gameObjectManager.Initialize();
            map = new Map(GameDevice.Instance());
            map.Load("Stage.csv");
            gameObjectManager.Add(map);
            tower = new Tower(new Vector2(Screen.Width, Screen.Height - 96 - 384), GameDevice.Instance());
            gameObjectManager.AddGameObject(tower);
            int width = 350;
            Rectangle bounds = new Rectangle(100, 100, width, 50);
            gauge = new Gauge(bounds, 100, 100, width, Color.GreenYellow,tower);
            player = new Player(new Vector2(100, 100), GameDevice.Instance(), gameObjectManager);
            gameObjectManager.AddGameObject(player);
            enemy = new Enemy(new Vector2(1800, 100), GameDevice.Instance(), gameObjectManager);
            gameObjectManager.AddGameObject(enemy);
        }

        /// <summary>
        /// 終了しているか？
        /// </summary>
        /// <returns></returns>
        public bool IsEnd()
        {
            return isEnd;
        }

        /// <summary>
        /// 次のシーンへ
        /// </summary>
        /// <returns></returns>
        public Scene Next()
        {
            return next;
        }

        /// <summary>
        /// 終了
        /// </summary>
        public void Shutdown()
        {

        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            gameObjectManager.Update(gameTime);
            //if (Input.GetKeyTrigger(Keys.D1))
            //{
            //    next = Scene.GameClear;
            //    isEnd = true;
            //}
            //else if (Input.GetKeyTrigger(Keys.D2))
            //{
            //    next = Scene.GameOver;
            //    isEnd = true;
            //}
            //else if(Input.GetKeyTrigger(Keys.P))
            //{
            //    next = Scene.Menu;
            //    isEnd = true;
            //}

            if (gameObjectManager.GetTower() == null)
            {
                next = Scene.GameOver;
                isEnd = true;
            }
            gauge.Update(gameTime);
        }

        /// <summary>
        /// 終了フラグ反転
        /// </summary>
        public void FlipEnd()
        {
            isEnd = !isEnd;
        }
    }
}
