﻿// このファイルで必要なライブラリのnamespaceを指定
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Team20.Def;
using Team20.Scene;
using Team20.Device;

/// <summary>
/// プロジェクト名がnamespaceとなります
/// </summary>
namespace Team20
{
    /// <summary>
    /// ゲームの基盤となるメインのクラス
    /// 親クラスはXNA.FrameworkのGameクラス
    /// </summary>
    public class Game1 : Game
    {
        // フィールド（このクラスの情報を記述）
        private GraphicsDeviceManager graphicsDeviceManager;//グラフィックスデバイスを管理するオブジェクト
        private SpriteBatch spriteBatch;//画像をスクリーン上に描画するためのオブジェクト

        private SceneManager sceneManager;
        private Renderer renderer;
        private GameDevice gameDevice;
        private Sound sound;

        /// <summary>
        /// コンストラクタ
        /// （new で実体生成された際、一番最初に一回呼び出される）
        /// </summary>
        public Game1()
        {
            //グラフィックスデバイス管理者の実体生成
            graphicsDeviceManager = new GraphicsDeviceManager(this);
            graphicsDeviceManager.PreferredBackBufferWidth = Screen.Width;
            graphicsDeviceManager.PreferredBackBufferHeight = Screen.Height;
            //graphicsDeviceManager.IsFullScreen = true;

            //コンテンツデータ（リソースデータ）のルートフォルダは"Contentに設定
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// 初期化処理（起動時、コンストラクタの後に1度だけ呼ばれる）
        /// </summary>
        protected override void Initialize()
        {
            // この下にロジックを記述

            //GameDevice.Instance(Content, GraphicsDevice);
            gameDevice = GameDevice.Instance(Content, GraphicsDevice);

            //シーン登録
            sceneManager = new SceneManager();
            sceneManager.Add(Scene.Scene.Title, new Title());
            sceneManager.Add(Scene.Scene.StageSelect, new StageSelect());
            //追加するシーンの変数を用意して、GamePlayの実体を生成して代入
            IScene addScene = new GamePlay();
            //シーン登録
            sceneManager.Add(Scene.Scene.GamePlay,addScene);
            sceneManager.Add(Scene.Scene.Config, new Config());
            sceneManager.Add(Scene.Scene.GameClear, new GameClear());
            sceneManager.Add(Scene.Scene.GameOver, new GameOver());
            sceneManager.Add(Scene.Scene.Result, new Result());
            sceneManager.Add(Scene.Scene.Menu, new Menu());
            sceneManager.Change(Scene.Scene.Title);
            // この上にロジックを記述
            base.Initialize();// 親クラスの初期化処理呼び出し。絶対に消すな！！
        }

        /// <summary>
        /// コンテンツデータ（リソースデータ）の読み込み処理
        /// （起動時、１度だけ呼ばれる）
        /// </summary>
        protected override void LoadContent()
        {
            // 画像を描画するために、スプライトバッチオブジェクトの実体生成
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // この下にロジックを記述

            renderer = gameDevice.GetRenderer();
            sound = gameDevice.GetSound();
            string filepath = "./Sound/";
            sound.LoadBGM("bgm01", filepath);
            sound.LoadBGM("bgm03", filepath);
            sound.LoadBGM("bgm04", filepath);
            sound.LoadBGM("bgm06", filepath);
            sound.LoadBGM("bgm07", filepath);
            sound.LoadBGM("bgm08", filepath);
            sound.LoadBGM("bgm09", filepath);
            sound.LoadBGM("bgm10", filepath);
            sound.LoadBGM("bgm11", filepath);
            sound.LoadBGM("bgm12", filepath);
            sound.LoadSE("jump_SE", filepath);
            sound.LoadSE("menu_SE", filepath);
            sound.LoadSE("punch_SE", filepath);
            sound.LoadSE("tap_SE", filepath);

            string path = "./Texture/";
            renderer.LoadContent("Title", path);
            renderer.LoadContent("Config", path);
            renderer.LoadContent("GameClear", path);
            renderer.LoadContent("GameOver", path);
            renderer.LoadContent("GamePlay", path);
            renderer.LoadContent("Result", path);
            renderer.LoadContent("StageSelect", path);
            renderer.LoadContent("block2", path);
            renderer.LoadContent("button1", path);
            renderer.LoadContent("button2", path);
            renderer.LoadContent("door1", path);
            renderer.LoadContent("door2", path);
            renderer.LoadContent("player", path);
            renderer.LoadContent("enemy", path);
            renderer.LoadContent("koin", path);
            renderer.LoadContent("money", path);
            renderer.LoadContent("number", path);
            renderer.LoadContent("Menu", path);
            renderer.LoadContent("tower", path);
            renderer.LoadContent("stage", path);
            renderer.LoadContent("cursor", path);
            renderer.LoadContent("Menu", path);
            renderer.LoadContent("BackGround", path);
            renderer.LoadContent("pause", path);
            renderer.LoadContent("gauge", path);
            renderer.LoadContent("pixel", path);
            renderer.LoadContent("inventory", path);
            renderer.LoadContent("sword", path);
            renderer.LoadContent("bow", path);
            renderer.LoadContent("pixel", path);
            renderer.LoadContent("BG", path);
            renderer.LoadContent("kusa", path);
            renderer.LoadContent("kusa2", path);
            renderer.LoadContent("tuti", path);
            renderer.LoadContent("tow", path);
            renderer.LoadContent("Boss", path);
            renderer.LoadContent("BossBullet", path);
            renderer.LoadContent("slime", path + "./Animation/");
            renderer.LoadContent("dash", path + "./Animation/");
            renderer.LoadContent("jump", path + "./Animation/");
            renderer.LoadContent("stand", path + "./Animation/");
            renderer.LoadContent("walk", path + "./Animation/");
            renderer.LoadContent("eyeof", path + "./Animation/");
            // この上にロジックを記述
        }

        /// <summary>
        /// コンテンツの解放処理
        /// （コンテンツ管理者以外で読み込んだコンテンツデータを解放）
        /// </summary>
        protected override void UnloadContent()
        {
            // この下にロジックを記述


            // この上にロジックを記述
        }

        /// <summary>
        /// 更新処理
        /// （1/60秒の１フレーム分の更新内容を記述。音再生はここで行う）
        /// </summary>
        /// <param name="gameTime">現在のゲーム時間を提供するオブジェクト</param>
        protected override void Update(GameTime gameTime)
        {
            // ゲーム終了処理（ゲームパッドのBackボタンかキーボードのエスケープボタンが押されたら終了）
            if ((GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed) ||
                 (Keyboard.GetState().IsKeyDown(Keys.Escape)))
            {
                Exit();
            }

            // この下に更新ロジックを記述
            sceneManager.Update(gameTime);
            gameDevice.Update(gameTime);

            // この上にロジックを記述
            base.Update(gameTime); // 親クラスの更新処理呼び出し。絶対に消すな！！
        }

        /// <summary>
        /// 描画処理
        /// </summary>
        /// <param name="gameTime">現在のゲーム時間を提供するオブジェクト</param>
        protected override void Draw(GameTime gameTime)
        {
            // 画面クリア時の色を設定
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // この下に描画ロジックを記述
            sceneManager.Draw(renderer);

            //Renderer renderer = GameDevice.Instance().GetRenderer();

            //renderer.Begin();

            //renderer.End();


            //この上にロジックを記述
            base.Draw(gameTime); // 親クラスの更新処理呼び出し。絶対に消すな！！
        }
    }
}
