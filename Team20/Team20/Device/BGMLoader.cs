﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Team20.Device
{
    /// <summary>
    ///BGMローダークラス
    /// </summary>
    class BGMLoader : Loader
    {
        private Sound sound;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="resources"></param>
        public BGMLoader(string[,] resources) : base(resources)
        {
            sound = GameDevice.Instance().GetSound();
            Initialize();
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            isEndFlag = true;
            if (counter < maxNum)
            {
                sound.LoadBGM(resources[counter, 0], resources[counter, 1]);
                counter += 1;
                isEndFlag = false;
            }
        }
    }
}
