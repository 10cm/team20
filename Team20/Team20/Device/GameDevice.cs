﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace Team20.Device
{
    /// <summary>
    /// ゲームデバイスクラス
    /// </summary>
    sealed class GameDevice
    {
        private static GameDevice instance;

        private Renderer renderer;
        private Sound sound;
        private static Random random;
        private ContentManager content;
        private GraphicsDevice graphics;

        public GameTime gameTime;
        private Vector2 displayModify;//ディスプレイ位置修正

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="content"></param>
        /// <param name="graphicsDevice"></param>
        public GameDevice(ContentManager content, GraphicsDevice graphics)
        {
            renderer = new Renderer(content, graphics);
            sound = new Sound(content);
            random = new Random();
            this.content = content;
            this.graphics = graphics;
            //displayModify = Vector2.Zero;
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public void Initialize()
        {

        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            Input.Update();
            this.gameTime = gameTime;
        }

        /// <summary>
        /// GameDeviceインスタンスの取得
        /// (Game1クラスで使う実体生成用)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="graphics"></param>
        /// <returns></returns>
        public static GameDevice Instance(ContentManager content,
            GraphicsDevice graphics)
        {
            if (instance == null)
            {
                instance = new GameDevice(content, graphics);
            }
            return instance;
        }

        /// <summary>
        /// インスタンスの取得
        /// </summary>
        /// <returns></returns>
        public static GameDevice Instance()
        {
            Debug.Assert(instance != null, "Game1クラスのInitializeメソッド内で引数付きInstanceメソッドを生成してください");
            return instance;
        }

        /// <summary>
        /// レンダラーオブジェクトの取得
        /// </summary>
        /// <returns></returns>
        public Renderer GetRenderer()
        {
            return renderer;
        }

        /// <summary>
        /// 乱数オブジェクトの取得
        /// </summary>
        /// <returns></returns>
        public Random GetRandom()
        {
            return random;
        }

        /// <summary>
        /// サウンドオブジェクトの取得
        /// </summary>
        /// <returns></returns>
        public Sound GetSound()
        {
            return sound;
        }

        /// <summary>
        /// コンテンツ管理者の取得
        /// </summary>
        /// <returns></returns>
        public ContentManager GetContentManager()
        {
            return content;
        }

        /// <summary>
        /// ゲーム時間の取得
        /// </summary>
        /// <returns></returns>
        public GameTime GetGameTime()
        {
            return gameTime;
        }

        /// <summary>
        /// 画面のズレ修正
        /// </summary>
        /// <param name="position"></param>
        public void SetDisplayModify(Vector2 position)
        {
            displayModify = position;
        }

        /// <summary>
        /// ズレ修正の値を取得
        /// </summary>
        /// <returns></returns>
        public Vector2 GetDisplayModify()
        {
            return displayModify;
        }
    }
}
