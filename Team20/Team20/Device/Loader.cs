﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace Team20.Device
{
    /// <summary>
    /// コンテンツローダー抽象クラス
    /// </summary>
    abstract class Loader
    {
        protected string[,] resources;
        protected int counter;
        protected int maxNum;
        protected bool isEndFlag;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="resources"></param>
        public Loader(string[,] resources)
        {
            this.resources = resources;
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public void Initialize()
        {
            counter = 0;
            isEndFlag = false;
            maxNum = 0;

            Debug.Assert(resources != null, "リソースデータ情報が間違っています");
            maxNum = resources.GetLength(0);
        }

        /// <summary>
        /// 最大登録数
        /// </summary>
        /// <returns></returns>
        public int RegistMAXNum()
        {
            return maxNum;
        }

        /// <summary>
        /// 現在の登録している番号を取得
        /// </summary>
        /// <returns></returns>
        public int CurrentCount()
        {
            return counter;
        }

        /// <summary>
        /// 終了か？
        /// </summary>
        /// <returns></returns>
        public bool IsEnd()
        {
            return isEndFlag;
        }

        /// <summary>
        /// 抽象更新
        /// </summary>
        /// <param name="gameTime"></param>
        public abstract void Update(GameTime gameTime);
    }
}
