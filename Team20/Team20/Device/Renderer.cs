﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace Team20.Device
{
    /// <summary>
    /// レンダラークラス
    /// </summary>
    class Renderer
    {
        private ContentManager contentManager;
        private GraphicsDevice graphicsDevice;
        private SpriteBatch spriteBatch;
        private RenderTarget2D renderTarget2D;

        private Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="contentManager"></param>
        /// <param name="graphicsDevice"></param>
        public Renderer(ContentManager contentManager, GraphicsDevice graphicsDevice)
        {
            this.contentManager = contentManager;
            this.graphicsDevice = graphicsDevice;
            spriteBatch = new SpriteBatch(graphicsDevice);
        }

        /// <summary>
        /// 画像読み込み
        /// </summary>
        /// <param name="assetName"></param>
        /// <param name="filepath"></param>
        public void LoadContent(string assetName, string filepath = "./")
        {
            if (textures.ContainsKey(assetName))
            {
#if DEBUG
                Console.WriteLine(assetName + "はすでに読み込まれています。\n プログラムを確認してください。");
#endif
                return;
            }
            textures.Add(assetName, contentManager.Load<Texture2D>(filepath + assetName));
        }

        /// <summary>
        /// 画像読み込み（画像オブジェクト版）
        /// </summary>
        /// <param name="assetName"></param>
        /// <param name="texture2D"></param>
        public void LoadContent(string assetName, Texture2D texture2D)
        {
            if (textures.ContainsKey(assetName))
            {
#if DEBUG
                Console.WriteLine(assetName + "はすでに読み込まれています。\n プログラムを確認してください。");
#endif
                return;
            }
            textures.Add(assetName, texture2D);
        }

        /// <summary>
        ///  アンロード
        /// </summary>
        public void UnLoad()
        {
            textures.Clear();
        }

        /// <summary>
        /// 描画開始
        /// </summary>
        public void Begin()
        {
            spriteBatch.Begin();
        }

        /// <summary>
        /// 描画終了
        /// </summary>
        public void End()
        {
            spriteBatch.End();
        }

        #region 画像の描画関連
        public void DrawTexture(string assetName, Vector2 position, float alpha = 1.0f)
        {
            spriteBatch.Draw(textures[assetName], position, Color.White * alpha);
        }

        public void DrawTexture(string assetName, Vector2 position, Rectangle rect, float alpha = 1.0f)
        {
            spriteBatch.Draw(textures[assetName], position, rect, Color.White * alpha);
        }

        public void DrawTexture(string assetName, Vector2 position, Rectangle rect, float rotate, Vector2 rotatePosition, Vector2 origin, float scale, SpriteEffects effects = SpriteEffects.None, float depth = 0.0f, float alpha = 1.0f)
        {
            spriteBatch.Draw(textures[assetName], position, rect, Color.White * alpha, rotate, origin, scale, effects, depth);
        }

        public void DrawTexture(string asserName,Rectangle rect, Color color)
        {
            spriteBatch.Draw(textures[asserName], rect, color);
        }

        public void DrawNumber(string assetName, Vector2 position, int number, float alpha = 1.0f)
        {
            Debug.Assert(textures.ContainsKey(assetName), "描画時にアセット名の指定を間違えたか、画像の読み込み自体出来ていません");

            if (number < 0)
            {
                number = 0;
            }

            int width = 32;

            foreach (var n in number.ToString())
            {
                spriteBatch.Draw(textures[assetName], position, new Rectangle((n - '0') * width, 0, width, 64), Color.White * alpha);
            }
        }
        #endregion 数字の描画
    }
}
