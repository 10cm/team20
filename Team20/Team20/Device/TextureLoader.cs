﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Team20.Device
{
    /// <summary>
    /// テクスチャローダークラス
    /// </summary>
    class TextureLoader : Loader
    {
        private Renderer renderer;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="resources"></param>
        public TextureLoader(string[,] resources) : base(resources)
        {
            renderer = GameDevice.Instance().GetRenderer();
            Initialize();
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            isEndFlag = true;

            if (counter < maxNum)
            {
                renderer.LoadContent(resources[counter, 0], resources[counter, 1]);
                counter += 1;
                isEndFlag = false;
            }
        }
    }
}
