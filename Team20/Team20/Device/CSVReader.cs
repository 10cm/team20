﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team20.Device
{
    /// <summary>
    /// CSV読み込みクラス
    /// </summary>
    class CSVReader
    {
        private List<string[]> stringData;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CSVReader()
        {
            stringData = new List<string[]>();
        }

        /// <summary>
        /// CSVファイルの読み込み
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="path"></param>
        public void Read(string filename, string path = "./")
        {
            stringData.Clear();

            //例外設定
            try
            {
                //csvファイルを開く
                using (var sr = new System.IO.StreamReader(@"Content/" + path + filename))
                {
                    //ストリームの末尾まで繰り返す
                    while (!sr.EndOfStream)
                    {
                        //1行読み込む
                        var line = sr.ReadLine();
                        //カンマごとに分けて配列に格納する
                        var values = line.Split(',');//文字のカンマ

                        //リストに読み込んだ1行を追加
                        stringData.Add(values);
#if DEBUG
                        //出力する
                        foreach (var v in values)
                        {
                            System.Console.Write("{0}", v);
                        }
                        System.Console.WriteLine();
#endif
                    }
                }
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine(e.Message);
            }
        }

        public void Clear()
        {
            stringData.Clear();
        }

        public List<string[]> GetData()
        {
            return stringData;
        }

        public string[][] GetArrayData()
        {
            return stringData.ToArray();
        }

        public int[][] GetIntData()
        {
            var data = GetArrayData();

            int row = data.Count();

            int[][] intData = new int[row][];

            for (int i = 0; i < row; i++)
            {
                int col = new int[i].Count();
                intData[i] = new int[col];
            }
            for (int y = 0; y < row; y++)
            {
                for (int x = 0; x < intData[y].Count(); x++)
                {
                    intData[y][x] = int.Parse(data[y][x]);
                }
            }

            return intData;
        }

        public string[,] GetStringMatrix()
        {
            var data = GetArrayData();
            int row = data.Count();
            int col = data[0].Count();

            string[,] result = new string[row, col];

            for (int y = 0; y < row; y++)
            {
                for (int x = 0; x < col; x++)
                {
                    result[y, x] = data[y][x];
                }
            }
            return result;
        }

        public int[,] GetIntMatrix()
        {
            var data = GetIntData();
            int row = data.Count();
            int col = data[0].Count();

            int[,] result = new int[row, col];

            for (int y = 0; y < row; y++)
            {
                for (int x = 0; x < col; x++)
                {
                    result[y, x] = data[y][x];
                }
            }
            return result;
        }
    }
}
