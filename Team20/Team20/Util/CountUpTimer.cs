﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Team20.Util
{
    /// <summary>
    /// カウントアップタイマークラス
    /// </summary>
    class CountUpTimer : Timer
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CountUpTimer()
        {
            Initialize();
        }

        /// <summary>
        /// コンストラクタ(秒数指定)
        /// </summary>
        /// <param name="second">制限時間</param>
        public CountUpTimer(float second) : base(second)
        {
            Initialize();
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize()
        {
            currentTime = 0;
        }

        /// <summary>
        /// 時間になったか？
        /// </summary>
        /// <returns></returns>
        public override bool IsTime()
        {
            return currentTime >= limitTime;
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            currentTime = Math.Min(currentTime + 1f, limitTime);
        }

        /// <summary>
        /// レート
        /// </summary>
        /// <returns></returns>
        public override float rate()
        {
            return currentTime / limitTime;
        }

    }
}
