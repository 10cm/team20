﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Team20.Util
{
    /// <summary>
    /// タイマー抽象クラス
    /// </summary>
    abstract class Timer
    {
        protected float limitTime;
        protected float currentTime;

        /// <summary>
        /// コンストラクタ(秒数指定)
        /// </summary>
        /// <param name="second"></param>
        public Timer(float second)
        {
            limitTime = second * 60;
        }

        /// <summary>
        /// コンストラクタ(引数なしは1秒)
        /// </summary>
        public Timer() : this(1)
        {

        }

        public abstract void Initialize();//初期化

        public abstract void Update(GameTime gameTime);//更新

        public abstract bool IsTime();//時間になったか？

        /// <summary>
        /// タイマーを設定
        /// </summary>
        /// <param name="Second"></param>
        public void SetTimer(float Second)
        {
            limitTime = 60 * Second;
        }

        /// <summary>
        /// 現在の秒数
        /// </summary>
        /// <returns></returns>
        public float Now()
        {
            return currentTime / 60f;
        }

        public abstract float rate();
    }
}
