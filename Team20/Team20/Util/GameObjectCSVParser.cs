﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Team20.Actor;
using Team20.Device;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace Team20.Util
{
    class GameObjectCSVParser
    {
        private CSVReader csvReader;
        private List<GameObject> gameObjects;
        //デリケート宣言（メソッドを変数に保存するための型宣言）
        //戻り値の型がGameObject、引数はList<string>のメソッドを
        //保存できる1Function型を宣言
        private delegate GameObject IFunction(List<string> data);
        //文字列と１Function型をディクショナリーで保存
        private Dictionary<string, IFunction> functionTable;

        public GameObjectCSVParser()
        {
            csvReader = new CSVReader();
            gameObjects = new List<GameObject>();
            functionTable = new Dictionary<string, IFunction>();
            functionTable.Add("Block", NewBlock);
            //functionTable.Add("enemy", NewEnemy);
        }

        public List<GameObject> Parse(string filename, string path = "./")

        {
            gameObjects.Clear();

            csvReader.Read(filename, path);
            var data = csvReader.GetData();

            foreach (var line in data)
            {
                if (line[0] == "#")
                {
                    continue;
                }
                if (line[0] == "")
                {
                    continue;
                }

                var temp = line.ToList();
                temp.RemoveAll(s => s == "");

                gameObjects.Add(functionTable[line[0]](temp));
            }
            return gameObjects;
        }

        private Block NewBlock(List<string> data)
        {
            Debug.Assert(
                (data.Count == 3),
                "CSVデータを確認してください。");

            return new Block(
                new Vector2(float.Parse(data[1]), float.Parse(data[2])) * 32,
                GameDevice.Instance());
        }
        //private Enemy NewEnemy(List<string>data)
        //{
        //    Debug.Assert(
        //        (data==))
        //}
    }
}
//GameObjectCSVParser parser = new GameObjectCSVParser();

//    var dataList = parser.Parse("GameObjectParameter.csv", "./csv/");
//        foreach(var data in dataList)
//            {
//           gameObjectManager.Add(data);
//            }
//    }
