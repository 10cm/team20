﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Team20.Util
{
    /// <summary>
    /// カウントダウンタイマークラス
    /// </summary>
    class CountDownTimer : Timer
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CountDownTimer()
        {
            Initialize();
        }

        /// <summary>
        /// コンストラクタ(秒数指定)
        /// </summary>
        /// <param name="second">制限時間</param>
        public CountDownTimer(float second) : base(second)
        {
            Initialize();
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize()
        {
            currentTime = limitTime;

        }

        /// <summary>
        /// 時間になったか？
        /// </summary>
        /// <returns></returns>
        public override bool IsTime()
        {
            return currentTime <= 0.0f;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            currentTime = Math.Max(currentTime - 1.0f, 0.0f);
        }

        /// <summary>
        /// レート
        /// </summary>
        /// <returns></returns>
        public override float rate()
        {
            return 1.0f - currentTime / limitTime;
        }

       
    }
}
